package com.spring_storedprocedure.controller;

import com.spring_storedprocedure.model.Ticket;
import com.spring_storedprocedure.repository.TicketRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TicketController {
    @Autowired
    TicketRepo ticketRepo;

    @GetMapping("/getTickets")
    public List<Ticket> getTickets(){
       return ticketRepo.getTicketInfo();
    }

    @GetMapping("/getTickets/{category}")
    public List<Ticket> getTicketsByCategory(@PathVariable String category){
        return ticketRepo.getTicketInfoByCategory(category);
    }
}
