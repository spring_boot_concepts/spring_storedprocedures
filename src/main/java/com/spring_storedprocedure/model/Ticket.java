package com.spring_storedprocedure.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.servlet.tags.Param;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NamedStoredProcedureQueries({@NamedStoredProcedureQuery(name = "firstProcedure", procedureName = "getTickets"),
        @NamedStoredProcedureQuery(name = "secondProcedure", procedureName = "geTicketsByCategory", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "tcategory", type = String.class)
        })})
public class Ticket {
    @Id
    private int id;
    private int amount;
    private String category;
}
