package com.spring_storedprocedure.repository;

import com.spring_storedprocedure.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TicketRepo {
    @Autowired
    EntityManager manager;

    public List<Ticket> getTicketInfo() {
        return manager.createNamedStoredProcedureQuery("firstProcedure").getResultList();
    }
    public List<Ticket> getTicketInfoByCategory(String input){
        return manager.createNamedStoredProcedureQuery("secondProcedure").setParameter("tcategory",input).getResultList();
    }
}
