package com.spring_storedprocedure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStoreProcedureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringStoreProcedureApplication.class, args);
	}

}
